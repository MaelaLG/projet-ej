extends Control

@onready var fb= $FirstBackground
@onready var anim = $FirstBackground/FirstText/AnimationPlayer

var isStart: bool
# Called when the node enters the scene tree for the first time.
func _ready():
	global.load_Game()
	if global.indexThem1 < 1:
		fb.visible=true
		anim.play()
		fb.set_process(true)
		isStart=true
	else :
		anim.stop()
		fb.visible=false
		fb.set_process(false)
		isStart=false


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_button_pressed():
	get_tree().change_scene_to_file("res://Scene/Menu.tscn")

func _on_ad_start_btn_pressed():
	fb.visible=true
	anim.stop()
	anim.play()
	fb.set_process(true)
	isStart=true
