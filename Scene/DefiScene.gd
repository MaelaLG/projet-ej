extends Control

@export var editDefi : Array[LineEdit]
@export var defiText: Array[RichTextLabel]
@export var btnText : Array[Button]
@onready var btnSave = $ValidateBtn
@onready var btnModify = $ModifyBtn
@onready var panel = $Panel
@onready var titlePanel = $Panel/TitlePanel
@onready var textPanel = $Panel/RichTextLabel

var indexbtn=0
var text

# Called when the node enters the scene tree for the first time.
func _ready():
	global.loadDefi()
	if !global.isSaveDefi:
		for x in range (4):
			defiText[x].visible = false
			editDefi[x].visible = false
			btnText[x].visible = true
		btnSave.visible = true
		btnModify.visible = false
	else :
		for x in range (4):
			defiText[x].visible = true
			editDefi[x].visible = false
			btnText[x].visible = false
		defiText[0].text= str(global.defiTxt)
		defiText[1].text = str(global.motivationTxt)
		defiText[2].text = str(global.prbTxt)
		defiText[3].text = str(global.sltTxt)
		btnText[0].text= str(global.defiTxt)
		btnText[1].text = str(global.motivationTxt)
		btnText[2].text = str(global.prbTxt)
		btnText[3].text = str(global.sltTxt)
		editDefi[0].text= str(global.defiTxt)
		editDefi[1].text = str(global.motivationTxt)
		editDefi[2].text = str(global.prbTxt)
		editDefi[3].text = str(global.sltTxt)
		btnSave.visible = false
		btnModify.visible = true
	panel.visible=false


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_menu_btn_pressed():
	get_tree().change_scene_to_file("res://Scene/Menu.tscn")


func _on_validate_btn_pressed():
	for x in range (4):
		if editDefi[x].text != "":
			defiText[x].text = editDefi[x].text
		defiText[x].visible = true
		editDefi[x].visible = false
		btnText[x].visible = false
	global.isSaveDefi=true
	btnModify.visible=true
	btnSave.visible=false
	panel.visible=false
	if defiText[0].text != "":
		global.defiTxt =defiText[0].text
	if defiText[1].text != "":
		global.motivationTxt = defiText[1].text
	if defiText[2].text != "":
		global.prbTxt = defiText[2].text
	if defiText[3].text != "":
		global.sltTxt = defiText[3].text
	global.saveDefi()


func _on_modify_btn_pressed():
	for x in range (4):
		defiText[x].visible = false
		editDefi[x].visible = false
		btnText[x].visible = true
	btnModify.visible=false
	btnSave.visible=true


func _on_exitbtn_pressed():
	match indexbtn:
		0:
				panel.visible=false
				editDefi[0].visible = false
				if editDefi[0].text == "" || editDefi[0].text == btnText[0].text:
					return
				else : 
						btnText[0].text= text
						editDefi[0].text=text
		1:
				panel.visible=false
				editDefi[1].visible = false
				if editDefi[1].text == "" || editDefi[1].text == btnText[1].text:
					return
				else : 
						btnText[1].text= text
						editDefi[1].text=text
		2:
				panel.visible=false
				editDefi[2].visible = false
				if editDefi[2].text == "" || editDefi[2].text == btnText[2].text:
					return
				else : 
						btnText[2].text= text
						editDefi[2].text=text
		3:
				panel.visible=false
				editDefi[3].visible = false
				if editDefi[3].text == "" || editDefi[3].text == btnText[3].text:
					return
				else : 
						btnText[3].text= text
						editDefi[3].text=text


func _on_defi_btn_pressed():
	panel.visible=true
	indexbtn=0
	editDefi[0].visible = true
	titlePanel.text= "Mon défi"
	textPanel.text = "Un rêve tout simple que je voudrais accomplir dans les prochaines semaines c'est de :"


func _on_motivation_btn_pressed():
	panel.visible=true
	indexbtn=1
	editDefi[1].visible = true
	titlePanel.text= "Motivations"
	textPanel.text = "Quand j'aurai réussi je me sentirai :"


func _on_prb_btn_pressed():
	panel.visible=true
	indexbtn=2
	editDefi[2].visible = true
	titlePanel.text= "Problèmes"
	textPanel.text = "Ce qui m'empêche d'accomplir mon rêve tout simple c'est que, pour l'instant,  j'ai parfois tendance à :"


func _on_slt_btn_pressed():
	panel.visible=true
	indexbtn=3
	editDefi[3].visible = true
	titlePanel.text= "Solutions"
	textPanel.text = "Alors je décide qu'à partir de maintenant, à chaque fois que je serai face à mon problème ou mes problèmes, je me dirai :"


func _on_line_edit_text_changed(new_text):
	text = new_text


func _on_line_edit_2_text_changed(new_text):
	text = new_text


func _on_line_edit_3_text_changed(new_text):
	text = new_text


func _on_line_edit_4_text_changed(new_text):
	text = new_text
