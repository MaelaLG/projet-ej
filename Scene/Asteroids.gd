extends CharacterBody2D

var asteroid1=preload("res://Prefabs/asteroid.tscn")
var asteroid2=preload("res://Prefabs/asteroid_2.tscn")
var asteroid3=preload("res://Prefabs/asteroid_3.tscn")
var asteroid4=preload("res://Prefabs/asteroid_4.tscn")
var asteroid5=preload("res://Prefabs/asteroid_5.tscn")
var asteroid6=preload("res://Prefabs/asteroid_6.tscn")
var asteroid7=preload("res://Prefabs/asteroid_7.tscn")
var asteroid8=preload("res://Prefabs/asteroid_8.tscn")
var asteroid9=preload("res://Prefabs/asteroid_9.tscn")
var asteroid10=preload("res://Prefabs/asteroid_10.tscn")

@export var moveSpeed = 100
@export var t : TextureRect

var rng= RandomNumberGenerator.new()
var timer : float
# Called when the node enters the scene tree for the first time.
func _ready():
	await get_tree().create_timer(1.0).timeout
	callAst()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	velocity.x = moveSpeed
	move_and_slide()
	if is_on_wall():
		moveSpeed = -moveSpeed

func callAst():
	var asteroids = [asteroid1, asteroid2, asteroid3, asteroid4, asteroid5, asteroid6, asteroid7, asteroid8, asteroid9, asteroid10]
	var asteroid = asteroids[randi()% asteroids.size()]
	var ast = asteroid.instantiate()
	t.add_child(ast)
	rng.randomize()
	timer= rng.randi_range(2,7)
	await get_tree().create_timer(timer).timeout
	callAst()
