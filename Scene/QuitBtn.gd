extends Button

@export var panel: Panel
@export var indexGlobal: int
@export var indexPanel : int

@export var sfxAudio : AudioStreamPlayer
@export var sound : AudioStream

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_pressed():
	sfxAudio.stream=sound
	sfxAudio.play()
	panel.visible = false
	if indexGlobal == 0:
		global.indexStoryPanel = indexPanel
	
	if indexGlobal == 1:
		global.indexArcadPanel = indexPanel
	
	if indexGlobal == 2:
		global.indexAdvicePanel = indexPanel
