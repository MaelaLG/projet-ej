extends Control

@onready var video = $VideoStreamPlayer
@onready var btnPlay = $Button2
@onready var btnMenu = $Button
@onready var btnPause= $PauseBtn
var rewind_increment = -5
# Called when the node enters the scene tree for the first time.
func _ready():
	btnPlay.visible=true

func _on_button_pressed():
	get_tree().change_scene_to_file("res://Scene/Menu.tscn")
	global.indexDialog=0

func _on_button_2_pressed():
	video.play()
	btnPlay.visible=false
	btnPause.text = "Pause"


func _on_video_stream_player_finished():
	btnPlay.visible=true
	if global.indexDialog==1:
		get_tree().change_scene_to_file("res://Scene/Dialogue1.tscn")


func _on_pause_btn_pressed():
	video.paused = !video.paused
	if video.paused:
		btnPause.text = "Jouer"
	else :
		btnPause.text = "Pause"


func _on_passed_btn_pressed():
		get_tree().change_scene_to_file("res://Scene/Dialogue1.tscn")
