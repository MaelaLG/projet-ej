extends Panel

@onready var btnDialog2 = $StartDialog2
@onready var dialogue2 = $DialogueBox2
@onready var panel = $DialogueBox2/ColorRect
@onready var label = $DialogueBox2/ColorRect/RichTextLabel
@onready var background = $TextureRect

# Called when the node enters the scene tree for the first time.
func _ready():
	panel.visible = false
	background.visible=false
	dialogue2.variables['gender'] = global.indexGender
	dialogue2.variables['name'] = global.nameplayer
	#background.visible=true
	dialogue2.variables['index'] = global.indexDialog
	if global.indexDialog>0:
		btnDialog2.visible=false
		dialogue2.start()
	if global.isStart:
		global.isStart = false
		global.save_player()
		btnDialog2.visible=false
		dialogue2.start()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):

func _on_start_dialog_2_pressed():
	btnDialog2.visible=false
	background.visible=true
	global.indexDialog=0
	dialogue2.variables['index'] = global.indexDialog
	dialogue2.start()


func _on_dialogue_box_2_dialogue_signal(value):
	if value == "ReStart":
		btnDialog2.visible=true
		global.indexDialog=0
		background.visible=false
		global.indexThem1=1
		global.save_Game()
	elif value == "StartCopy":
		dialogue2.copyDialog()
	elif value== "PanelOn":
		label.text = dialogue2.dialog
		panel.visible=true
	elif value=="PanelOff":
		panel.visible = false
	elif value == "ChangeScene":
		global.indexDialog=1
		get_tree().change_scene_to_file("res://Scene/TestVideo.tscn")
	elif value == "PLAY":
		global.indexDialog=2
		get_tree().change_scene_to_file("res://Scene/MemoryNumber.tscn")
	elif value == "PLAY2":
		global.indexDialog=3
		get_tree().change_scene_to_file("res://Scene/MemoryNumber.tscn")


func _on_quitbtn_pressed():
	get_tree().change_scene_to_file("res://Scene/Menu.tscn")
	global.indexDialog=0
