extends Panel


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
@export var keyboard : GridContainer
@export var lines : Array[LineEdit]
@export var labels: Array[RichTextLabel]
@export var sentences : Array[String]
@export var sentencesNumber : int
@export var normalColor : Color
@export var timer : float
@onready var result = $Result
@onready var btn = $Playbtn
@onready var btn2 = $Correctbtn
@onready var selectLine = $SelectLine
@onready var dialogue = $DialogueBox
@onready var adviceDialogue = $DialogueBox2
@onready var ruleBtn = $Rulebtn

var goodColor = Color(0.27451, 0.509804, 0.705882, 1)
var falseColor = Color(1, 0.498039, 0.313726, 1)
var my_styleG = StyleBoxFlat.new()
var my_styleF = StyleBoxFlat.new()
var array:=[]
var rng= RandomNumberGenerator.new()

var rngAdvice= RandomNumberGenerator.new()
var indexSentences : int

var saveFalse:=[]
var saveCorrection := []
var i = 0
var positionNumber=0
var lineNumber = 0
var goEdit = false
var count1: int = 0
var count2: int = 0
var count3: int = 0
var count4: int = 0
var count5: int = 0
var count6: int = 0
var count7: int = 0
var count8: int = 0
var count9: int = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	
	keyboard.visible=false
	goEdit = false
	ruleBtn.visible=true
	my_styleG.bg_color=goodColor
	my_styleF.bg_color = falseColor
	array.insert(0,"0")
	btn.visible=true
	btn2.visible=false
	selectLine.visible=false
	if global.indexThem1==0:
		if global.indexDialog != 3:
			dialogue.start()
	if global.indexDialog==3:
		btn.visible=false
		_on_playbtn_pressed()
	elif global.indexDialog==2:
		btn.visible=false
		dialogue.start()
	for x in range(15):
			lines[x].visible=false
			lines[x].text=""
			labels[x].visible=false
			lines[x].editable=true
			lines[x].modulate= normalColor
	#line.disabled=true

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if goEdit:
		if lines[positionNumber].text != "":
			if positionNumber<14:
				selectLine.set_position(Vector2(lines[positionNumber+1].position.x, lines[positionNumber+1].position.y))
				lines[positionNumber+1].grab_focus()
				positionNumber = positionNumber+1
			else :
				goEdit = false
				lineNumber=15
				selectLine.set_position(Vector2(btn.position.x-15, btn.position.y-5))
				return


func numberPlay(number:int, numberDisappear:int, isFinish: bool):
	rng.randomize()
	labels[number].text= str(rng.randi_range(1,9))
	while (labels[number].text==labels[number-1].text || labels[number].text==labels[number-2].text || labels[number].text==labels[number-3].text) || ((labels[number].text== "1" && count1 >1) || (labels[number].text== "2" && count2 >1) || (labels[number].text== "3" && count3 >1) || (labels[number].text== "4" && count4 >1) || (labels[number].text== "5" && count5 >1) || (labels[number].text== "6" && count6 >1) || (labels[number].text== "7" && count7 >1) || (labels[number].text== "8" && count8 >1) || (labels[number].text== "9" && count9 >1)) :
		rng.randomize()
		labels[number].text= str(rng.randi_range(1,9))
	counter(number)
	array.insert(number, labels[number].text)
	print(" array number : " + array[number])
	if !isFinish:
			labels[numberDisappear].visible=false
			labels[number].visible=true
	else:
		labels[numberDisappear].visible=false
		labels[number].visible=true
		await get_tree().create_timer(1.0).timeout
		GoPlay()

func counter(nb:int):
	match labels[nb].text:
		"1":
			count1=count1+1
		"2":
			count2=count2+1
		"3":
			count3=count3+1
		"4":
			count4=count4+1
		"5":
			count5=count5+1
		"6":
			count6=count6+1
		"7":
			count7=count7+1
		"8":
			count8=count8+1
		"9":
			count9=count9+1

func GoPlay():
	for x in range(15):
		lines[x].editable=true
		labels[x].visible=false
		lines[x].visible=true
		lines[x].set_process(true)
		lines[x].modulate= normalColor
	btn2.visible=true
	keyboard.visible=true
	selectLine.visible=true
	goEdit = true
	selectLine.set_position(Vector2(lines[positionNumber].position.x, lines[positionNumber].position.y))
	#lineSelection()
	selectLine.visible=true
	lines[0].grab_focus()


func _on_texture_button_2_pressed():
	btn.visible=true
	btn2.visible=false
	for x in range(i):
		lines[saveFalse[x]].text = saveCorrection[x]
	#else :
	#	correctionBtn.visible=true
	#	btn.visible=false
	#	btn2.visible=false

func _on_button_pressed():
	btn.visible=true
	btn2.visible=false
	for x in range(i):
		lines[saveFalse[x]].text = saveCorrection[x]


func _on_playbtn_pressed():
	result.text=""
	keyboard.visible=false
	selectLine.visible=false
	ruleBtn.visible=false
	count1=0
	count2=0
	count3=0
	count4=0
	count5=0
	count6=0
	count7=0
	count8=0
	count9=0
	for x in range(15):
		lines[x].max_length=1
		lines[x].visible=false
		lines[x].modulate = normalColor
		lines[x].clear()
		lines[x].text=""
		labels[x].text=""
	btn.visible=false
	var isFinish:bool
	for x in range(15):
			await get_tree().create_timer(timer).timeout 
			if x<14:
				isFinish=false
			else : isFinish=true
			numberPlay(x, x-1, isFinish)


func _on_correctbtn_pressed():
	selectLine.visible=false
	keyboard.visible=false
	ruleBtn.visible=true
	var countG = 0
	var countN = 0
	var numberFalse=0
	positionNumber=0
	selectLine.visible=false
	selectLine.set_position(Vector2(lines[0].position.x, lines[0].position.y))
	for x in range(15):
		lines[x].max_length=5
		if lines[x].text == array[x]:
			countG = countG+1
			#lines[x].modulate=goodColor
			lines[x].visible=false
			labels[x].visible=true
			labels[x].text = "[color=STEEL_BLUE] "+ array[x]
		else :
			countN = countN+1
			#lines[x].modulate= falseColor
			numberFalse=x
			saveFalse.insert(i, numberFalse)
			saveCorrection.insert(i, array[x])
			i=i+1
			lines[x].visible=false
			labels[x].text = "[color=CORAL]" + lines[x].text + "[color=ANTIQUE_WHITE]|" + array[x]
			labels[x].visible=true
			#lines[x].text = lines[x].text + " | " + array[x]
		lines[x].editable=false
	result.text= "[color=STEEL_BLUE] Chiffres bons [/color] : " + str(countG) #+ "  [color=CORAL] Chiffres faux [/color] : " +str(countN)
	if global.indexDialog<2:
		ruleBtn.visible = false
		btn2.visible=false
		btn.visible=false
		await get_tree().create_timer(5.0).timeout
		rngAdvice.randomize()
		indexSentences = rngAdvice.randi_range(0,sentencesNumber)
		adviceDialogue.variables['sentence'] = sentences[indexSentences]
		adviceDialogue.start()
	else :
		ruleBtn.visible = false
		btn2.visible=false
		btn.visible=false
		await get_tree().create_timer(5.0).timeout
		get_tree().change_scene_to_file("res://Scene/Dialogue1.tscn")


func _on_menubtn_pressed():
	global.indexDialog=0
	get_tree().change_scene_to_file("res://Scene/Menu.tscn")


func _on_line_edit_focus_entered():
	lineNumber=0

func _on_line_edit_2_focus_entered():
	lineNumber=1


func _on_line_edit_3_focus_entered():
	lineNumber=2


func _on_line_edit_4_focus_entered():
	lineNumber=3


func _on_line_edit_5_focus_entered():
	lineNumber=4


func _on_line_edit_6_focus_entered():
	lineNumber=5


func _on_line_edit_7_focus_entered():
	lineNumber=6


func _on_line_edit_8_focus_entered():
	lineNumber=7


func _on_line_edit_9_focus_entered():
	lineNumber=8


func _on_line_edit_10_focus_entered():
	lineNumber=9


func _on_line_edit_11_focus_entered():
	lineNumber=10


func _on_line_edit_12_focus_entered():
	lineNumber=11


func _on_line_edit_13_focus_entered():
	lineNumber=12


func _on_line_edit_14_focus_entered():
	lineNumber=13


func _on_line_edit_15_focus_entered():
	lineNumber=14

func _on_number_btn_pressed():
	if lineNumber<15:
		lines[lineNumber].text = "1"


func _on_number_btn_2_pressed():
	if lineNumber<15:
		lines[lineNumber].text = "2"


func _on_number_btn_3_pressed():
	if lineNumber<15:
		lines[lineNumber].text = "3"


func _on_number_btn_4_pressed():
	if lineNumber<15:
		lines[lineNumber].text = "4"


func _on_number_btn_5_pressed():
	if lineNumber<15:
		lines[lineNumber].text = "5"


func _on_number_btn_6_pressed():
	if lineNumber<15:
		lines[lineNumber].text = "6"


func _on_number_btn_7_pressed():
	if lineNumber<15:
		lines[lineNumber].text = "7"


func _on_number_btn_8_pressed():
	if lineNumber<15:
		lines[lineNumber].text = "8"


func _on_number_btn_9_pressed():
	if lineNumber<15:
		lines[lineNumber].text = "9"


func _on_dialogue_box_dialogue_signal(value):
	if value == "END":
		dialogue.visible=false
		if global.indexDialog ==2:
			_on_playbtn_pressed()


func _on_rulebtn_pressed():
	dialogue.start()


func _on_dialogue_box_2_dialogue_signal(value):
	if value == "END":
		btn.visible=true
		ruleBtn.visible = true
		btn2.visible=false
