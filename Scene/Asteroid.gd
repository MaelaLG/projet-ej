extends Sprite2D

@export var moveX : float
@export var moveY : float

var velocity = Vector2()
var rng= RandomNumberGenerator.new()
var rngSize= RandomNumberGenerator.new()
var indexSize:float
var speed:float

# Called when the node enters the scene tree for the first time.
func _ready():
	velocity=Vector2(-moveX, moveY)
	rng.randomize()
	speed= rng.randi_range(2,6)
	rngSize.randomize()
	indexSize = rngSize.randf_range(0.05, 0.25)
	scale= Vector2(indexSize,indexSize)
	await get_tree().create_timer(10.0).timeout
	die()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	translate(velocity*speed)

func die():
	queue_free()
