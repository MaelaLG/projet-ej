extends Control

@export var videos : Array[VideoStreamTheora]

@onready var playBtn = $PlayBtn
@onready var label = $Label
@onready var videoStream = $VideoStreamPlayer

# Called when the node enters the scene tree for the first time.
func _ready():
	match global.MCVideosindex:
		0:
			videoStream.stream = videos[0]
			label.text = "Minute Conseil 1"
		1:
			videoStream.stream = videos[1]
			label.text = "Minute Conseil 2"
		2:
			videoStream.stream = videos[2]
			label.text = "Minute Conseil 3"
		3:
			videoStream.stream = videos[3]
			label.text = "Minute Conseil 4"
		4:
			videoStream.stream = videos[4]
			label.text = "Minute Conseil 5"
		5:
			videoStream.stream = videos[5]
			label.text = "Minute Conseil 6"
		6:
			videoStream.stream = videos[6]
			label.text = "Minute Conseil 7"
		7:
			videoStream.stream = videos[7]
			label.text = "Minute Conseil 8"


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_play_btn_pressed():
	videoStream.play()
	playBtn.visible=false


func _on_menu_btn_pressed():
	get_tree().change_scene_to_file("res://Scene/Menu.tscn")
