extends Control

@export var padlocksT1 : Array[TextureRect]
@export var padlocksT2 : Array[TextureRect]
@export var padlocksT3 : Array[TextureRect]
@export var numberPadlocksT1 : int
@export var numberPadlocksT2 : int
@export var numberPadlocksT3 : int
@export var panels: Array[Panel] #a modifier pour 3 tableaux
@export var panelsStory : Array[Panel]
@export var panelsArcad : Array[Panel]
@export var panelsAdvice : Array[Panel]
@export var SFX : Array[AudioStream]
@export var numberPanels : int

@onready var labelTime = $TextureRect/Time

@onready var genderPanel = $TextureRect/GenderPanel
@onready var namePanel = $TextureRect/NamePanel
@onready var nameEdit = $TextureRect/NamePanel/LineEdit

@onready var optionPanel = $TextureRect/OptionPanel

@onready var musicSlider = $TextureRect/OptionPanel/MusicSlider
@onready var sfxSlider = $TextureRect/OptionPanel/SFXSlider
@onready var movieSlider = $TextureRect/OptionPanel/MovieSlider

@onready var sfxAudio = $AudioStreamSFX

var bus_master = AudioServer.get_bus_index("Music")
var bus_sfx = AudioServer.get_bus_index("SFX")
var bus_movie = AudioServer.get_bus_index("Movie")

# Called when the node enters the scene tree for the first time.
func _ready():
	print(global.indexGender)
	global.loadVolume()
	global.load_player()
	print(global.indexGender)
	if global.indexGender==0:
		genderPanel.visible=true
		namePanel.visible=false
	else :
		genderPanel.visible = false
		namePanel.visible=false
	#labelTime.text = Time.get_datetime_string_from_system(false)
	for x in range(numberPanels):
		panels[x].visible=false
	
	if global.indexAdvicePanel > 0:
		advicePanel()
	if global.indexArcadPanel > 0:
		arcadPanel()
	if global.indexStoryPanel > 0 :
		storyPanel()
	AudioServer.set_bus_volume_db(bus_master, global.musicVolume)
	AudioServer.set_bus_volume_db(bus_sfx, global.sfxVolume)
	AudioServer.set_bus_volume_db(bus_movie, global.movieVolume)
	musicSlider.value=AudioServer.get_bus_volume_db(bus_master)
	sfxSlider.value=AudioServer.get_bus_volume_db(bus_sfx)
	movieSlider.value=AudioServer.get_bus_volume_db(bus_movie)
	
	if global.indexThem1==1:
		for x in range(numberPadlocksT1):
			padlocksT1[x].visible = false
	elif global.indexThem1 == 0:
		for x in range(numberPadlocksT1):
			padlocksT1[x].visible = true

func storyPanel():
	if global.indexStoryPanel == 1:
		panelsStory[0].visible=true
	else :
		panelsStory[0].visible=true
		panelsStory[global.indexStoryPanel-1].visible=true

func arcadPanel():
	panelsArcad[0].visible=true

func advicePanel():
	if global.indexAdvicePanel == 1:
		panelsAdvice[0].visible=true
	else :
		panelsAdvice[0].visible=true
		panelsAdvice[global.indexAdvicePanel-1].visible=true

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_story_btn_pressed():
	panelsStory[0].visible=true


func _on_arcade_btn_pressed():
	panelsArcad[0].visible=true


func _on_conseil_btn_pressed():
	panelsAdvice[0].visible=true
	global.indexAdvicePanel=1


func _on_defi_btn_pressed():
	get_tree().change_scene_to_file("res://Scene/Defi.tscn")


func _on_them_1_btn_pressed():
	panelsStory[1].visible=true
	global.indexStoryPanel=2


func _on_ch_1t_1_btn_pressed():
	get_tree().change_scene_to_file("res://Scene/Dialogue1.tscn")


func _on_button_pressed():
	global.indexArcadPanel=1
	get_tree().change_scene_to_file("res://Scene/MemoryNumber.tscn")


func _on_ava_btn_pressed():
	get_tree().change_scene_to_file("res://Scene/TestVideo.tscn")


func _on_minutes_conseils_btn_pressed():
	panelsAdvice[1].visible=true
	global.indexAdvicePanel=2


func _on_mc_btn_1_pressed():
	get_tree().change_scene_to_file("res://Scene/MCVideos.tscn")
	global.MCVideosindex=0


func _on_mc_btn_2_pressed():
	get_tree().change_scene_to_file("res://Scene/MCVideos.tscn")
	global.MCVideosindex=1


func _on_mc_btn_3_pressed():
	get_tree().change_scene_to_file("res://Scene/MCVideos.tscn")
	global.MCVideosindex=2


func _on_mc_btn_4_pressed():
	get_tree().change_scene_to_file("res://Scene/MCVideos.tscn")
	global.MCVideosindex=3


func _on_mc_btn_5_pressed():
	get_tree().change_scene_to_file("res://Scene/MCVideos.tscn")
	global.MCVideosindex=4


func _on_mc_btn_6_pressed():
	get_tree().change_scene_to_file("res://Scene/MCVideos.tscn")
	global.MCVideosindex=5


func _on_mc_btn_7_pressed():
	get_tree().change_scene_to_file("res://Scene/MCVideos.tscn")
	global.MCVideosindex=6


func _on_mc_btn_8_pressed():
	get_tree().change_scene_to_file("res://Scene/MCVideos.tscn")
	global.MCVideosindex=7


func _on_quit_btn_pressed():
	optionPanel.visible=false
	sfxAudio.stream = SFX[0]
	sfxAudio.play()


func _on_optionbtn_pressed():
	optionPanel.visible=true


func _on_music_slider_value_changed(value):
	AudioServer.set_bus_volume_db(bus_master, value)
	AudioServer.set_bus_mute(bus_master, value == musicSlider.min_value)
	global.musicVolume=value
	global.saveVolume()


func _on_sfx_slider_value_changed(value):
	AudioServer.set_bus_volume_db(bus_sfx, value)
	AudioServer.set_bus_mute(bus_sfx, value == sfxSlider.min_value)
	global.sfxVolume = value
	global.saveVolume()


func _on_movie_slider_value_changed(value):
	AudioServer.set_bus_volume_db(bus_movie, value)
	AudioServer.set_bus_mute(bus_movie, value == movieSlider.min_value)
	global.movieVolume=value
	global.saveVolume()


func _on_mbtn_pressed():
	genderPanel.visible=false
	global.indexGender=1
	namePanel.visible=true


func _on_fbtn_pressed():
	genderPanel.visible=false
	global.indexGender=2
	namePanel.visible=true


func _on_name_btn_pressed():
	global.nameplayer= nameEdit.text
	namePanel.visible=false
	get_tree().change_scene_to_file("res://Scene/Dialogue1.tscn")
	global.save_player()


func _on_resetbtn_pressed():
	global.indexGender=0
	global.nameplayer= ""
	global.isStart = true
	global.indexThem1 = 0
	global.indexThem2 = 0
	global.indexThem3 = 0
	global.save_player()
	global.save_Game()
	get_tree().change_scene_to_file("res://Scene/Start.tscn")


func _on_ch_2t_1_btn_2_pressed():
	get_tree().change_scene_to_file("res://Scene/Dialogue2T1.tscn")
