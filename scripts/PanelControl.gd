extends Control

@onready var genderPanel = $Panel/ColorRect
@onready var panelDialog = $Panel4
@onready var panelStory = $Panel2
@onready var panelArcad = $Panel3
@onready var labels= [$Panel3/Label, $Panel3/Label3, $Panel3/Label4, $Panel3/Label5]
@onready var btn= $Panel3/TextureButton2
@onready var lines = [$Panel3/LineEdit, $Panel3/LineEdit2, $Panel3/LineEdit3, $Panel3/LineEdit4]

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
func _ready():
	if global.indexGender==0:
		genderPanel.visible=true
	else:
		genderPanel.visible=false
	panelArcad.set_process(false)
	panelStory.set_process(false)
	panelDialog.set_process(false)
	panelArcad.visible=false
	panelStory.visible=false
	panelDialog.visible=false
	if global.indexDialog==0:
		pass
	else:
		panelDialog.visible=true


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_Menubtn2_pressed():
	panelArcad.visible=false
	panelArcad.set_process(false)
	btn.visible=false
	for x in range(4):
		labels[x].text= ""
		lines[x].clear()
		lines[x].visible=false
		lines[x].set_process(false)


func _on_Menubtn1_pressed():
	panelStory.visible=false
	panelStory.set_process(false)


func _on_Storybtn2_pressed():
	panelArcad.visible=true
	panelArcad.set_process(true)


func _on_Storybtn_pressed():
	panelStory.visible=true
	panelStory.set_process(true)



func _on_dialogbtn_pressed():
	panelDialog.visible=true
	panelDialog.set_process(true)


func _on_quitbtn_pressed():
	panelDialog.visible=false
	panelDialog.set_process(false)


func _on_mbtn_pressed():
	genderPanel.visible=false
	global.indexGender=1
	global.save_game()


func _on_fbtn_pressed():
	genderPanel.visible=false
	global.indexGender=2
	global.save_game()


func _on_reset_genderbtn_pressed():
	global.indexGender=0
	global.save_game()
