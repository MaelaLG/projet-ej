extends Control

@export var SFX : Array[AudioStream]


@onready var optionPanel = $OptionPanel

@onready var musicSlider = $OptionPanel/MusicSlider
@onready var sfxSlider = $OptionPanel/SFXSlider
@onready var movieSlider = $OptionPanel/MovieSlider

var bus_master = AudioServer.get_bus_index("Music")
var bus_sfx = AudioServer.get_bus_index("SFX")
var bus_movie = AudioServer.get_bus_index("Movie")

# Called when the node enters the scene tree for the first time.
func _ready():
	musicSlider.min_value=-35
	musicSlider.step=0.001
	sfxSlider.min_value=-35
	sfxSlider.step=0.001
	movieSlider.min_value=-35
	movieSlider.step=0.001
	global.loadVolume()
	AudioServer.set_bus_volume_db(bus_master, global.musicVolume)
	AudioServer.set_bus_volume_db(bus_sfx, global.sfxVolume)
	AudioServer.set_bus_volume_db(bus_movie, global.movieVolume)
	musicSlider.value=AudioServer.get_bus_volume_db(bus_master)
	sfxSlider.value=AudioServer.get_bus_volume_db(bus_sfx)
	movieSlider.value=AudioServer.get_bus_volume_db(bus_movie)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass



func _on_optionbtn_pressed():
	optionPanel.visible=true
	optionPanel.set_process(true)


func _on_quit_btn_pressed():
	optionPanel.visible=false
	optionPanel.set_process(false)


func _on_music_slider_value_changed(value):
	AudioServer.set_bus_volume_db(bus_master, value)
	AudioServer.set_bus_mute(bus_master, value == musicSlider.min_value)
	global.musicVolume=value
	global.saveVolume()


func _on_sfx_slider_value_changed(value):
	AudioServer.set_bus_volume_db(bus_sfx, value)
	AudioServer.set_bus_mute(bus_sfx, value == sfxSlider.min_value)
	global.sfxVolume = value
	global.saveVolume()



func _on_movie_slider_value_changed(value):
	AudioServer.set_bus_volume_db(bus_movie, value)
	AudioServer.set_bus_mute(bus_movie, value == movieSlider.min_value)
	global.movieVolume=value
	global.saveVolume()
