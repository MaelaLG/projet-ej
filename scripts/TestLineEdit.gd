extends Panel


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
@export var lines : Array[LineEdit]
@export var labels: Array[Label]
@export var goodColor: Color
@export var falseColor : Color
@export var normalColor : Color
@export var timer : float
@onready var result = $Result
@onready var btn = $TextureButton
@onready var btn2 = $TextureButton2
@onready var correctionBtn = $Button
@onready var selectLine = $SelectLine

var my_styleG = StyleBoxFlat.new()
var my_styleF = StyleBoxFlat.new()
var array:=[]
var rng= RandomNumberGenerator.new()

var saveFalse:=[]
var saveCorrection := []
var i = 0
var positionNumber=0
var goEdit = false

# Called when the node enters the scene tree for the first time.
func _ready():
	goEdit = false
	my_styleG.bg_color=goodColor
	my_styleF.bg_color = falseColor
	array.insert(0,"0")
	correctionBtn.visible=false
	btn.visible=true
	btn2.visible=false
	selectLine.visible=false
	for x in range(15):
			lines[x].visible=false
			lines[x].text=""
			labels[x].visible=false
			lines[x].editable=true
			lines[x].modulate= normalColor
	#line.disabled=true

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if goEdit:
		if lines[positionNumber].text != "":
			if positionNumber<14:
				selectLine.set_position(Vector2(lines[positionNumber+1].position.x, lines[positionNumber+1].position.y))
				positionNumber = positionNumber+1
			else :
				goEdit = false
				selectLine.visible=false
				return


func _on_TextureButton_pressed():
	result.text=""
	selectLine.visible=false
	for x in range(15):
		lines[x].max_length=1
		lines[x].visible=false
		lines[x].clear()
		lines[x].text=""
	btn.visible=false
	var isFinish:bool
	for x in range(15):
			await get_tree().create_timer(timer).timeout 
			if x<14:
				isFinish=false
			else : isFinish=true
			numberPlay(x, x-1, isFinish)

func numberPlay(number:int, numberDisappear:int, isFinish: bool):
	rng.randomize()
	labels[number].text= str(rng.randi_range(1,9)) # Replace with function body.
	array.insert(number, labels[number].text)
	print(" array number : " + array[number])
	if !isFinish:
			labels[numberDisappear].visible=false
			labels[number].visible=true
	else:
		labels[numberDisappear].visible=false
		labels[number].visible=true
		await get_tree().create_timer(1.0).timeout
		GoPlay()

func GoPlay():
	for x in range(15):
		lines[x].editable=true
		labels[x].visible=false
		lines[x].visible=true
		lines[x].set_process(true)
		lines[x].modulate= normalColor
	btn2.visible=true
	selectLine.visible=true
	goEdit = true
	selectLine.set_position(Vector2(lines[positionNumber].position.x, lines[positionNumber].position.y))
	#lineSelection()


func _on_texture_button_2_pressed():
	var countG = 0
	var countN = 0
	var numberFalse=0
	positionNumber=0
	selectLine.set_position(Vector2(lines[0].position.x, lines[0].position.y))
	for x in range(15):
		lines[x].max_length=5
		if lines[x].text == array[x]:
			countG = countG+1
			lines[x].modulate=goodColor
		else :
			countN = countN+1
			lines[x].modulate= falseColor
			numberFalse=x
			saveFalse.insert(i, numberFalse)
			saveCorrection.insert(i, array[x])
			i=i+1
			lines[x].text = lines[x].text + " | " + array[x]
		lines[x].editable=false
	result.text= "[color=STEEL_BLUE] Chiffres bons [/color] : " + str(countG) + "  [color=ORANGE_RED] Chiffres faux [/color] : " +str(countN)
	#if countN==0:
	correctionBtn.visible=false
	btn.visible=true
	btn2.visible=false
	#else :
	#	correctionBtn.visible=true
	#	btn.visible=false
	#	btn2.visible=false



func _on_button_pressed():
	correctionBtn.visible=false
	btn.visible=true
	btn2.visible=false
	for x in range(i):
		lines[saveFalse[x]].text = saveCorrection[x]
