extends Node

var nameplayer
var MCVideosindex : int = 0
var indexStoryPanel: int = 0
var indexArcadPanel: int = 0
var indexAdvicePanel: int = 0
var indexDialog:int = 0
var indexGender: int = 0
var save_gender = "user://gender.save"
var save_Defi = "user://defiSave.save"
var isSaveDefi : bool
var defiTxt = ""
var motivationTxt = ""
var prbTxt = ""
var sltTxt = ""
var dataDefi = {}
var dataCharacter = {}
var isStart: bool = true

var musicVolume : float =0
var sfxVolume : float =0
var movieVolume : float=0
var dataVolume = {}
var save_Volume = "user://volumeSave.save"


var indexThem1: int = 0
var indexThem2:int = 0
var indexThem3: int = 0
var dataGame = {}
var save_game = "user://game.save"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func save_Game():
	var file = FileAccess.open(save_game, FileAccess.WRITE)
	dataGame = {
		"indexThem1" = indexThem1,
		"indexThem2" = indexThem2,
		"indexThem3" = indexThem3
		}
	file.store_var(dataGame)
	file = null

func load_Game():
	if not FileAccess.file_exists(save_game):
		var file = FileAccess.open(save_game, FileAccess.READ)
		dataGame = {
		"indexThem1" = 0,
		"indexThem2" = 0,
		"indexThem3" = 0
		}
		save_Game()
	var file = FileAccess.open(save_game, FileAccess.READ)
	dataGame = file.get_var()
	indexThem1 = dataGame.indexThem1
	indexThem2 = dataGame.indexThem2
	indexThem3 = dataGame.indexThem3
	file = null

func save_player():
	var file = FileAccess.open(save_gender, FileAccess.WRITE)
	dataCharacter = {
		"indexGender" = indexGender,
		"nameplayer" = nameplayer,
		"isStart" = isStart
		}
	file.store_var(dataCharacter)
	file = null

func load_player():
	if not FileAccess.file_exists(save_gender):
		var file = FileAccess.open(save_gender, FileAccess.READ)
		dataCharacter = {
		"indexGender" = 0,
		"nameplayer" = "nom",
		"isStart" = true
		}
		save_player()
	var file = FileAccess.open(save_gender, FileAccess.READ)
	dataCharacter = file.get_var()
	indexGender = dataCharacter.indexGender
	nameplayer = dataCharacter.nameplayer
	isStart = dataCharacter.isStart
	file = null

func loadDefi():
	if not FileAccess.file_exists(save_Defi):
		dataDefi = {
			"isSaveDefi" = false,
			"defiTxt" = "",
			"motivationTxt" = "",
			"prbTxt" = "",
			 "sltTxt" = ""
		}
		saveDefi()
	var file = FileAccess.open(save_Defi, FileAccess.READ)
	dataDefi = file.get_var()
	isSaveDefi = dataDefi.isSaveDefi
	defiTxt = dataDefi.defiTxt
	motivationTxt = dataDefi.motivationTxt
	prbTxt = dataDefi.prbTxt
	sltTxt = dataDefi.sltTxt
	file = null

func saveDefi():
	var file = FileAccess.open(save_Defi, FileAccess.WRITE)
	dataDefi = {
		"isSaveDefi" = isSaveDefi,
		"defiTxt" = defiTxt,
		"motivationTxt" = motivationTxt,
		"prbTxt" = prbTxt,
		 "sltTxt" = sltTxt}
	file.store_var(dataDefi)
	file = null

func loadVolume():
	if not FileAccess.file_exists(save_Volume):
		dataVolume = {
			"musicVolume" = 0,
			"sfxVolume" = 0,
			 "movieVolume" = 0
		}
		saveVolume()
	var file = FileAccess.open(save_Volume, FileAccess.READ)
	dataVolume = file.get_var()
	musicVolume = dataVolume.musicVolume
	sfxVolume = dataVolume.sfxVolume
	movieVolume = dataVolume.movieVolume
	file = null

func saveVolume():
	var file = FileAccess.open(save_Volume, FileAccess.WRITE)
	dataVolume = {
		"musicVolume" = musicVolume,
		"sfxVolume" = sfxVolume,
		 "movieVolume" = movieVolume}
	file.store_var(dataVolume)
	file = null
