extends Panel


var save_path = "user://savetest.save"
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

@onready var btnSave= $TBtnSave
@onready var btnMod= $TBtnMod
@onready var line= $LineEdit
@onready var label = $Label

var isSave=false
var textLine


# Called when the node enters the scene tree for the first time.
func _ready():
	if FileAccess.file_exists(save_path):
		var file = FileAccess.open(save_path, FileAccess.READ)
		isSave = file.get_var(isSave)
		print("label : " + label.text)
		label.text=file.get_line()
		print(isSave)
		print("label2 : " + label.text)
	if isSave:
		btnMod.visible=true
		btnSave.visible=false
		line.visible=false
		label.visible=true
	else :
		btnMod.visible=false
		btnSave.visible=true
		line.visible=true
		label.visible=false
	textLine=""


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_line_edit_text_changed(new_text):
	textLine = new_text

func _on_TBtnSave_pressed():
	label.text= textLine
	btnMod.visible=true
	btnSave.visible=false
	line.visible=false
	label.visible=true
	isSave=true
	var file = FileAccess.open(save_path, FileAccess.WRITE)
	file.store_var(isSave)
	file.store_string(label.text)


func _on_TBtnMod_pressed():
	btnMod.visible=false
	btnSave.visible=true
	line.visible=true
	label.visible=false




func _on_texture_button_pressed():
	isSave = false;
	var file = FileAccess.open(save_path, FileAccess.WRITE)
	file.store_var(isSave)
	get_tree().change_scene_to_file("res://Scene/Start.tscn")
