extends Resource
class_name Character

@export var name : String = ''
@export var fileName: String = ''
@export var image : Texture2D
